import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    role: 'admin'
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
