import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
Vue.use(VueRouter);


const routes = [
  // {
  //   path: "/",
  //   name: "home",
  //   component: HomeView,
  // },
  // {
  //   path: "/about",
  //   name: "about",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  // },
  {
    path: "/",
    redirect: '/test1',
    component: () => import("../views/test1.vue"),
  },
  {
    path: "/test1",
    name: "test1",
    component: () => import("../views/test1.vue"),
  },
  {
    path: "/test2",
    name: "test2",
    component: () => import("../views/test2.vue"),
  },
  {
    path: "/signUp",
    name: "signUp",
    component: () => import("../views/signUp.vue"),
    beforeEnter: (to, from, next) => {
      if (store.state.role == 'admin') { next() } else {
        next({ name: 'notFound', replace: true })
      }
    }
  },
  {
    path: "/test4",
    name: "test4",
    component: () => import("../views/test4.vue"),
  },
  {
    path: "/notFound",
    name: "notFound",
    component: () => import("../views/notFound.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
